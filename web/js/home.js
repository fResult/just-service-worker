;(function Home() {
  'use strict'

  let postsList

  document.addEventListener('DOMContentLoaded', ready, false)

  // **********************************

  function ready() {
    postsList = document.getElementById('my-posts')
    console.log('postsList', postsList)
    main().catch(console.error)
  }

  async function main() {
    let postIDs

    try {
      const res = await fetch('/api/get-posts')
      if (res && res.ok) {
        console.log('res', res)
        postIDs = await res.json()
        console.log('postIDs', postIDs)
      }
    } catch (err) {}

    renderPostIDs(postIDs || [])
  }

  function renderPostIDs(postIDs) {
    if (postIDs.length > 0) {
      postsList.innerHTML = ''
      for (let postID of postIDs) {
        let [, year, month, day, postNum] = String(postID).match(
          /^(\d{4})(\d{2})(\d{2})(\d+)$/
        )
        let postEntry = document.createElement('li')
        postEntry.innerHTML = `<a href="/post/${postID}">Post-${+month}/${+day}/${year}-${postNum}</a>`
        postsList.appendChild(postEntry)
      }
    } else {
      postsList.innerHTML = '<li>-- nothing yet, check back soon! --</li>'
    }
  }
})()
