;(function Blog() {
  'use strict'

  let offlineIcon
  let isOnline = 'onLine' in navigator ? navigator.onLine : true
  let isLoggedIn = /isLoggedIn=1/.test(document.cookie.toString() || '')
  let usingServiceWorker = 'serviceWorker' in navigator
  let swRegistration
  let serviceWorker

  if (usingServiceWorker) {
    initServiceWorker().catch(console.error)
  }

  window.global.isBlogOnline = isBlogOnline

  document.addEventListener('DOMContentLoaded', ready, false)

  // **********************************

  /**
   * @return { void }
   */
  function ready() {
    offlineIcon = document.getElementById('connectivity-status')

    if (!isOnline) {
      offlineIcon.classList.remove('hidden')
    }

    window.addEventListener('online', function online() {
      offlineIcon.classList.add('hidden')
      isOnline = true
      sendStatusUpdate()
    })

    window.addEventListener('offline', function offline() {
      offlineIcon.classList.remove('hidden')
      isOnline = false
      sendStatusUpdate()
    })
  }

  function isBlogOnline() {
    return isOnline
  }

  async function initServiceWorker() {
    swRegistration = await navigator.serviceWorker.register('/sw.js', {
      updateViaCache: 'none'
    })
    serviceWorker =
      swRegistration.installing ||
      swRegistration.waiting ||
      swRegistration.active

    navigator.serviceWorker.addEventListener(
      'controllerchange',
      function onController() {
        serviceWorker = navigator.serviceWorker.controller
        sendStatusUpdate(serviceWorker)
      }
    )

    navigator.serviceWorker.addEventListener('message', onSwMessage)
  }

  /**
   * @param { ServiceWorkerMessageEvent } e
   */
  function onSwMessage(e) {
    const { data } = e
    if (data.requestStatusUpdate) {
      console.log(
        'Received status update request from service worker, responding...'
      )
      sendStatusUpdate(e.ports && e.ports[0])
    } else if (data === 'force-logout'){
      isLoggedIn = false
      sendStatusUpdate()
    }
  }

  /**
   * @param { MessagePort } target
   */
  function sendStatusUpdate(target) {
    sendSwMessage({ statusUpdate: { isOnline, isLoggedIn } }, target)
  }

  /**
   * @param { any } msg
   * @param { MessagePort } target
   * @return { Promise<void> }
   */
  function sendSwMessage(msg, target) {
    if (target) {
      target.postMessage(msg)
    } else if (serviceWorker) {
      serviceWorker.postMessage(msg)
    } else {
      navigator.serviceWorker.controller.postMessage(msg)
    }
  }
})()
