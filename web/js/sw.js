'use strict'

if ('function' === typeof window)
  importScripts('/js/external/idb-keyval-iife.min.js')

const version = 8
let isOnline = true
let isLoggedIn = false
const cacheName = `ramblings-${version}`
let allPostCaching = false

const urlsToCache = {
  loggedOut: [
    '/',
    '/about',
    '/contact',
    '/login',
    '/404',
    '/offline',
    '/css/style.css',
    '/js/add-post.js',
    '/js/blog.js',
    '/js/home.js',
    '/js/login.js',
    '/images/logo.gif',
    '/images/offline.png'
  ]
}

self.addEventListener('install', onInstall)
self.addEventListener('activate', onActivate)
self.addEventListener('message', onMessage)
self.addEventListener('fetch', onFetch)

main().catch(console.error)

/* ****************************************** */

/** @return { Promise<void> } */
async function main() {
  await sendMessage({ statusUpdateRequest: true })
  await cacheLoggedOutFiles()
  return cacheAllPosts()
}

/**
 * @param e { ExtendableEvent }
 * @return { Promise<void> }
 */
async function onInstall(e) {
  console.log(`Service Worker (${version}) has installed.`)
  await self.skipWaiting()
}

async function sendMessage(msg) {
  /** @type { Clients[] } allClients */
  let allClients = await clients.matchAll({ includeUncontrolled: true })
  return Promise.all(
    allClients.map(function clientMsg(client) {
      let channel = new MessageChannel()
      channel.port1.onmessage = onMessage
      return client.postMessage(msg, [channel.port2])
    })
  )
}

function onMessage({ data }) {
  if (data.statusUpdate) {
    const { isOnline, isLoggedIn } = data.statusUpdate
    console.log(
      `Service Worker: (v${version}) status update, isOnline: ${isOnline}, isLoggedIn: ${isLoggedIn}`
    )
  }
}

/** @param { FetchEvent } e */
function onFetch(e) {
  e.respondWith(router(e.request))
}

/** @param { Request } req */
async function router(req) {
  let url = new URL(req.url)
  let reqURL = url.pathname
  let cache = await caches.open(cacheName)

  // Request for site own URL
  if (url.origin === location.origin) {
    // are we making API request ?
    if (reqURL.match(/^\/api\/.+$/)) {
      const fetchOptions = {
        method: 'GET',
        headers: 'same-origin',
        cache: 'no-store'
      }
      let res = await safeRequest(
        reqURL,
        req,
        fetchOptions,
        /* cacheResponse = */ false,
        /* checkCacheFirst = */ false,
        /* checkCacheLast = */ true,
        /* useRequestDirectly = */ true
      )
      if (res) {
        if (req.method === 'GET') {
          await cache.put(reqURL, res.clone())
        }
        // Clear offline-backup of successful post ?
        else if (reqURL === '/api/add-post') {
          await idbKeyval.del('add-post-backup')
        }
        return res
      }

      return notFoundResponse()
    }
    // Are we requesting a page ?
    else if (req.headers.get('Accept').includes('text/html')) {
      // Login aware request
      if (reqURL.match(/^\/(?:login|logout|add-post)$/)) {
        let res

        if (reqURL === '/login') {
          if (isOnline) {
            const fetchOptions = {
              method: req.method,
              headers: req.headers,
              credentials: 'same-origin',
              cache: 'no-store',
              redirect: 'manual'
            }
            res = await safeRequest(reqURL, req, fetchOptions /* TODO */)
            if (res) {
              if (res.type === 'opaqueredirect') {
                return Response.redirect('/add-post', 307)
              }
              return res
            }

            if (isLoggedIn) {
              return Response.redirect('/add-post', 307)
            }
            res = await cache.match('/login')
            if (res) return res
            return Response.redirect('/', 307)
          } else if (/*!isOnline && */ isLoggedIn) {
            return Response.redirect('/add-post', 307)
          } else {
            res = await cache.match('/login')
            if (res) return res
            return cache.match('/offline')
          }
        } else if (reqURL === '/logout') {
          if (isOnline) {
            let fetchOptions = {
              method: req.method,
              headers: req.headers,
              credentials: 'same-origin',
              cache: 'no-store',
              redirect: 'manual'
            }
            res = await safeRequest(reqURL, req, fetchOptions)
            if (res) {
              if (res.type === 'opaqueredirect') {
                return Response.redirect('/', 307)
              }
              return res
            }

            if (isLoggedIn) {
              isLoggedIn = false
              await sendMessage('force-logout')
              await delay(100)
            }
            return Response.redirect('/', 307)
          } else if (isLoggedIn) {
            isLoggedIn = false
            await delay(100)
            return Response.redirect('/', 307)
          } else {
            return Response.redirect('/', 307)
          }
          // TODO: Maybe continue this
        } else if (reqURL === '/add-post') {
          if (isOnline) {
            const fetchOptions = {
              method: req.method,
              headers: req.headers,
              credentials: 'same-origin',
              cache: 'no-store'
            }
            res = await safeRequest(reqURL, req, fetchOptions)
            if (res) return res
            res = await cache.match(isLoggedIn ? '/add-post' : '/login')
            if (res) return res
            return Response.redirect('/', 307)
          } else if (isLoggedIn) {
            res = await cache.match('/add-post')
            if (res) return res
            return cache.match('/offline')
          } else {
            res = await cache.match('/login')
            if (res) return res
            return cache.match('/offline')
          }
        }
      }
      // Otherwise, just use network-and-cache
      else {
        let fetchOptions = {
          method: req.method,
          headers: req.headers,
          cache: 'no-store'
        }
        let res = await safeRequest(
          reqURL,
          req,
          fetchOptions,
          /* cacheResponse = */ false,
          /* checkCacheFirst = */ false,
          /* checkCacheLast = */ true
        )
        if (res) {
          if (!res.headers.get('X-Not-Found')) {
            await cache.put(reqURL, res.clone())
          }
          return res
        }

        // Otherwise, return an offline-friendly page
        return cache.match('/offline')
      }
    }
    //  All other files use 'cache-first'
    else {
      const fetchOptions = {
        method: req.method,
        headers: req.headers,
        cache: 'no-cache'
      }

      const res = await safeRequest(
        reqURL,
        req,
        fetchOptions,
        /* cacheResponse = */ true,
        /* checkCacheFirst = */ true
      )
      if (res) return res
    }
    // Otherwise, force a network-level 404 response
    return notFoundResponse()
  }
}

/**
 * @param { number } ms
 */
function delay(ms) {
  return new Promise(function c(resolve) {
    setTimeout(resolve, ms)
  })
}

async function safeRequest(
  reqURL,
  req,
  options,
  cacheResponse = false,
  checkCacheFirst = false,
  checkCacheLast = false,
  useRequestDirectly = false
) {
  let cache = await caches.open(cacheName)
  let res

  if (checkCacheFirst) {
    res = await cache.match(reqURL, cache)
    if (res) return res
  }

  if (isOnline) {
    try {
      if (useRequestDirectly) {
        res = await fetch(req, options)
      } else {
        res = await fetch(req.url, options)
      }

      if (res && (res.ok || res.type === 'opaqueredirect')) {
        if (cacheResponse) {
          await cache.put(reqURL, res.clone())
        }
        return res
      }
    } catch (err) {}
  }

  if (checkCacheLast) {
    res = await cache.match(reqURL)
    if (res) return res
  }
}

function notFoundResponse() {
  return new Response('', {
    status: 404,
    statusText: 'Not Found'
  })
}

/**
 * @param e { ExtendableEvent }
 * @return { void }
 */
function onActivate(e) {
  e.waitUntil(handleActivation())
}

async function handleActivation() {
  await clearCaches()
  await cacheLoggedOutFiles(/*forceReload = */ true)
  await clients.claim()
  console.log(`Service Worker (${version}) has activated.`)
}

async function clearCaches() {
  let cacheNames = await caches.keys()
  const oldCacheNames = cacheNames.filter(function matchOldCache(cacheName) {
    if (cacheName.match(/^ramblings-\d+$/)) {
      let [, cacheVersion] = cacheName.match(/^ramblings-(\d+)$/)
      cacheVersion = cacheVersion !== null ? Number(cacheVersion) : cacheVersion

      return cacheVersion > 0 && Number(cacheVersion) !== version
    }
  })

  return Promise.all(
    oldCacheNames.map(function deleteCache(cacheName) {
      return caches.delete(cacheName)
    })
  )
}

async function cacheLoggedOutFiles(forceReload = false) {
  const cache = caches.open(cacheName)

  return Promise.all(
    urlsToCache.loggedOut.map(async function requestFile(url) {
      try {
        let res
        if (!forceReload) {
          res = await cache.match(url)
          if (res) {
            return
          }
        }

        let fetchOptions = {
          method: 'GET',
          cache: 'no-cache',
          credentials: 'omit'
        }

        res = await fetch(url, fetchOptions)
        if (res.ok) {
          await cache.put(url, res)
        }
      } catch (err) {}
    })
  )
}

async function cacheAllPosts(forceReload = false) {
  if (allPostCaching) return
  allPostCaching = true
  await delay(5000)

  const cache = caches.open(cacheName)
  let postIDs

  try {
    if (isOnline) {
      const fetchOptions = {
        method: 'GET',
        cache: 'no-store',
        credentials: 'omit'
      }

      let res = await fetch('/api/get-posts', fetchOptions)
      if (res && res.ok) {
        await cache.put('/api/get-posts', fetchOptions)
        postIDs = await res.json()
      }
    } else {
      let res = await cache.match('/api/get-posts')
      if (res) {
        let resCopy = res.clone()
        postIDs = await res.json()
      }
      // Caching is not started, try to start again (later)
      else {
        allPostCaching = false
        return cacheAllPosts(forceReload)
      }
    }
  } catch (err) {
    console.error(err)
  }

  if (postIDs.length > 0) {
    return cachePost(postIDs.shift())
  } else {
    allPostCaching = false
  }

  async function cachePost(postID) {
    const postURL = `/post/${postID}`
    let needCaching = true

    if (!forceReload) {
      const res = await cache.match(postURL)
      if (res) {
        needCaching = true
      }

      if (needCaching) {
        await delay(10000)
        if (isOnline) {
          try {
            const fetchOptions = {
              method: 'GET',
              cache: 'no-store',
              credentials: 'omit'
            }

            let res = await fetch(postURL, fetchOptions)
            if (res && res.ok) {
              await cache.put(postURL, res.clone())
              needCaching = false
            }
          } catch (err) {}
        }

        // Failed, try caching this post again ?
        if (needCaching) {
          return cachePost(postID)
        }
      }

      // Anymore posts to cache ?
      if (postIDs.length > 0) {
        return cachePost(postIDs.shift())
      } else {
        allPostCaching = false
      }
    }
  }
}
